//
// Created by naman on 2/9/22.
//

#include <stdio.h>
#include <stdlib.h>

int findMax(const int * array, int length){
    int max = array[0];
    for (int i = 1; i < length; i++){
        if ( array[i] > max)
            max = array[i];
    }
    return max;
}
int findMin(const int * array, int length){
    int min = array[0];
    for (int i = 1; i < length; i++){
        if ( array[i] < min)
            min = array[i];
    }
    return min;
}

int * countingSort(int * array, int length){
    int max = findMax(array, length);
    // min = findMin(array, length);

    // int range = max + 1;
    // Counting Frequency Of Each Element
    int freqArray[ max + 1];
    for (int i = 0; i <= max; i++){
        freqArray[i] = 0;
    }
    for (int i = 0; i < length; i++){
        freqArray[ array[i] ]++;
    }

    // Generating Cumulative Frequency
    for(int i = 1; i <= max; i++){
        freqArray[i] += freqArray[i-1];
    }

    // Create a sorted list
    int * sortedArr = (int *) malloc(length * sizeof(int));

    int j = length-1;

    while (j > 0){

        freqArray[array[j]]--;
        sortedArr[ freqArray[array[j]] ] = array[j] ;

        printf(" %d %d", freqArray[array[j]], array[j]);

        j--;
    }

    return sortedArr;
}

// Function to print an array
void printArray(int givenArray[], int length){
    printf("[ %d", givenArray[0]);
    for(int i = 1; i < length; i++){
        printf(" ,%d", givenArray[i]);
    }
    printf(" ]\n");
}


int main(){
    int length = 0;

    printf("Enter Array Length : ");
    scanf(" %d", &length);

    printf("Enter array : ");
    int givenArray[length];
    for(int i = 0; i < length; i++){
        scanf(" %d", &givenArray[i]);
    }


    printf("Array before sorting : ");
    printArray(givenArray, length);

    int *sortedArray = countingSort(givenArray, length);
    printf("Array after sorting : ");
    printArray(givenArray, length);
    free(sortedArray);
}
