//
// Created by naman on 12/8/22.
//

#include <stdio.h>

void InsertionSort(int givenArray[], int length){
    for(int i = 1; i < length; i++){
        int key = givenArray[i];
        int j = i;
        while (j > 0 && key < givenArray[j-1]){
            givenArray[j] = givenArray[j-1];
            j--;
        }
        givenArray[j] = key;
    }
}


// Function to print an array
void printArray(int givenArray[], int length){
    printf("[ %d", givenArray[0]);
    for(int i = 1; i < length; i++){
        printf(" ,%d", givenArray[i]);
    }
    printf(" ]\n");
}

int main(){
    int len = 0;
    printf("Enter length of Array : ");
    scanf(" %d", &len);
    printf("Enter array : ");
    int givenArray[len];
    for (int i = 0; i < len; i++){
        scanf(" %d", &givenArray[i]);
    }
    printf("\nArray before sorting : ");
    printArray(givenArray, len);

    printf("\nArray after sorting : ");
    InsertionSort(givenArray, len);
    printArray(givenArray, len);
}