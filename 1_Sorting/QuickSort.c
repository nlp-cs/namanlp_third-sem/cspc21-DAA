//
// Created by naman on 26/8/22.
//

#include <stdio.h>

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

int partition(int * array, int start, int end){
    int pivot = array[end];
    int i = start-1;

    for (int j= start; j <= end-1; j++){
        if(array[j] <=pivot){
            i++;
            swap( &array[i], &array[j]);
        }
    }
    swap(&array[i+1], &array[end]);
    return (i+1);
}

void quickSort(int * array , int start, int end){
    if ( start < end){
        int part = partition( array, start, end);
        quickSort(array, start, part-1);
        quickSort(array, part + 1, end);
    }
}

// Function to print an array
void printArray(int givenArray[], int length){
    printf("[ %d", givenArray[0]);
    for(int i = 1; i < length; i++){
        printf(" ,%d", givenArray[i]);
    }
    printf(" ]\n");
}


int main(){

    int sizeOfArray = 0;
    printf("Enter Size of array : ");
    scanf(" %d", &sizeOfArray);

    printf("Enter array : ");
    int array[sizeOfArray];
    for (int i= 0; i<sizeOfArray; i++){
        scanf(" %d", &array[i]);
    }

    printf("\nArray before sorting : ");
    printArray(array, sizeOfArray);

    quickSort(array, 0, sizeOfArray-1);
    printf("Array after sorting : ");
    printArray(array, sizeOfArray);
}