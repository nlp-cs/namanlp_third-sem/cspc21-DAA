//
// Created by naman on 12/8/22.
//

#include <stdio.h>

void swap(int * a, int * b){
    int temp = *a;
    *a = *b;
    *b  = temp;
}

void SelectionSort(int givenArray[], int length){
    for(int i = 0; i < length-1; i++){
        int min = i;
        for (int j = i+1; j < length; j++ ){
            if(givenArray[j] < givenArray[min]){
                min = j;
            }
        }
        swap(&givenArray[i], &givenArray[min]);
    }
}

// Function to print an array
void printArray(int givenArray[], int length){
    printf("[ %d", givenArray[0]);
    for(int i = 1; i < length; i++){
        printf(" ,%d", givenArray[i]);
    }
    printf(" ]\n");
}

int main(){
    int len = 0;
    printf("Enter length of Array : ");
    scanf(" %d", &len);
    printf("Enter array : ");
    int givenArray[len];
    for (int i = 0; i < len; i++){
        scanf(" %d", &givenArray[i]);
    }
    printf("\nArray before sorting : ");
    printArray(givenArray, len);

    printf("\nArray after sorting : ");
    SelectionSort(givenArray, len);
    printArray(givenArray, len);
}