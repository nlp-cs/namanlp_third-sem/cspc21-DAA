#include <stdio.h>

// Function to print an array
void printArray(int givenArray[], int length){
    printf("[ %d", givenArray[0]);
    for(int i = 1; i < length; i++){
        printf(" ,%d", givenArray[i]);
    }
    printf(" ]\n");
}

void swap(int * a, int * b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void maxHeapify(int array[], int length, int root){
    int left = 2*root+1;
    int right = 2*root+2;
    int max = root;

    if ( left < length && array[root] < array[left]){
        max = left;
    }
    if ( right < length && array[max] < array[right]){
        max = right;
    }

    if ( max != root){
        swap(&array[max], &array[root]);
        maxHeapify(array, length, max);
    }
}

void buildMaxHeap(int array[], int length){
    // We know that last n/2 elements are leaf nodes

    for (int i = length/2 + 1; i >=0; i-- ){
        maxHeapify(array, length, i);
    }
}

void heapSort(int array[], int length){
    buildMaxHeap(array, length);

    for(int i = 0; i < length; i++){
        swap(&array[0], &array[length-i-1]);
        maxHeapify(array, length-i-1, 0);
    }
}

int main(){
    int len = 0;
    printf("Enter length of Array : ");
    scanf(" %d", &len);
    printf("Enter array : ");
    int givenArray[len];
    for (int i = 0; i < len; i++){
        scanf(" %d", &givenArray[i]);
    }
    printf("\nArray before sorting : ");
    printArray(givenArray, len);

    printf("\nArray after sorting : ");
    heapSort(givenArray, len);
    printArray(givenArray, len);
}
