//
// Created by naman on 2/9/22.
//

#include<stdio.h>

char stack[100];
int top=-1;

int isEmpty(){
    if(top==-1)return 1;
    else return 0;
}
int isFull(){
    if(top>99){
        return 1;
    }
    return 0;
}
void pop(){
    if(!isEmpty()){
        top=top-1;
    }
    else{
        printf("Stack empty");
    }
}
void push(char data){
    if( !isFull() ){
        top+=1;
        stack[top]=data;
    }
    else{
        printf("Stack is full");
    }
}

// Return Precedence of operator
int pre(char a){
    if(a=='-'||a=='+')return 1;
    if(a=='*'||a=='/')return 2;
    if(a=='^')return 3;
    return 0;
}
int main(){
    int length = 0;
    printf("Enter maximum length of string : ");
    scanf(" %d", &length);
    printf("Enter string : ");
    char givenString[length];
    scanf(" %s", givenString);

    for(int i=0;i<length;i++){
        // ignore whitespaces
        if (givenString[i] == ' '){
            continue;
        }
        // reached end of string
        if (givenString[i] == '\0'){
            break;
        }

        // Print Operands
        if(!pre(givenString[i]) && givenString[i]!=')'&& givenString[i]!='('){
            printf(" %c ",givenString[i]);
        }

        // Handel Parenthesis
        else if(givenString[i]=='('){
            push(givenString[i]);
        }

        else if(givenString[i]==')'){
            while(stack[top]!='('){
                printf("%c" ,stack[top]);
                pop();
            }
            pop();
        }

        // Pushing operators
        else{
            while(!isEmpty() && pre(stack[top])>=pre(givenString[i])){
                printf("%c",stack[top]);
                pop();
            }
            push(givenString[i]);
        }
    }
    // finally, empty the stack
    while(!isEmpty()){
        printf(" %c" ,stack[top]);
        pop();
    }
}