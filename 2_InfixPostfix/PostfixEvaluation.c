//
// Created by naman on 9/9/22.
//
// We assume it is a valid postfix expression with positive integer numbers and all operators and operands are space separtaed.
//
// For example, "43 55 +" is a valid postfix expression and returns 98 

#include <stdio.h>

int main(){
    printf("Enter the postfix expression ( upto 1000 chars ) : ");
    int i = 0;
    int number = 0;
    int isNumber = 0;
    int stack[1000];
    int top = 0;

    char input;
    // Not actually taking input, and processing it.
    // Rather processing as the characters are entered 

    while ( input != '\n' && i != 999){
        scanf("%c", &input);

        if(input <='9' && input >='0'){
            // For multi digit numbers
            number = number*10 + (input-48);
            isNumber = 1;

        } else if( input == ' ' && isNumber){
            stack[top] = number;
            top++;
            isNumber = 0;
            number = 0;
        } else if (input == '+' ){
            top--;
            stack[top-1] = stack[top-1] + stack[top];
        }else if (input == '-' ){
            top--;
            stack[top-1] = stack[top-1] - stack[top];
        }else if (input == '/' ){
            top--;
            stack[top-1] = stack[top-1] / stack[top];
        }
        else if (input == '*' ){
            top--;
            stack[top-1] = stack[top-1] * stack[top];
        }
        i++;
    }
    printf("Result is : %d ", stack[0]);

}
