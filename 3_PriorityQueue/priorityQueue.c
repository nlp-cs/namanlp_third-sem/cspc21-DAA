//
// Created by naman on 23/10/22.
//

#include <stdio.h>

struct process{
    int priority;
    int processID;
};

// To increase Priority

struct process increasePriority(struct process queue[], int length, int processID, int newPriority){
    int index = -1;
    for (int i = 0; i < length; i++){
        if(queue[i].processID == processID){
            index = i;
            break;
        }
    }
    // If process not found
    if(index == -1){
        struct process temp;
        temp.processID = -1;
        temp.priority = -1;
        printf("\nSorry, process no found!");
        return temp;
    }
    // If process found
    while (index > 0 && queue[(index-1)/2].priority < newPriority){
        queue[index].processID = queue[(index-1)/2].processID;
        queue[index].priority = queue[(index-1)/2].priority;

        index = (index-1)/2;
    }

    queue[index].priority = newPriority;
    queue[index].processID = processID;

    return  queue[index];
}

// To insert Process in priority queue

struct process insertInQueue(struct process queue[], int *length, int maxLength, int processID, int priority){

    // If queue filled
    if((*length)+1 >= maxLength){
        struct process temp;
        temp.processID = -1;
        temp.priority = -1;
        printf("\nSorry, queue is filled!");
    }

    queue[*length].processID = processID;
    queue[*length].priority = -1000;
    (*length)++;
    printf("\nInserted");

    return increasePriority(queue, *length, processID, priority);
}

struct process peakIntoQueue(struct process queue[], int length){
    if(length<1){
        printf("\nSorry, empty queue");
        struct process temp;
        temp.processID = -1;
        temp.priority = -1;
        return temp;
    }
    return queue[0];
}



struct process extractMax(struct process queue[], int *length){
    if(*length<1){
        printf("\nSorry, empty queue");
        struct process temp;
        temp.processID = -1;
        temp.priority = -1;
        return temp;
    }

    struct process temp = queue[0];
    (*length)--;

    // Replace with larger child, and iteratively all its child

    int index = 0;
    while (index < *length){
        int tempIndex = index;
        // leftChild = queue[(index*2)+1];
        if((index*2)+1 < *length){
            queue[index] = queue[(index*2)+1];
            tempIndex = index*2 +1;
        }

        // rightChild = queue[(index*2)+2];
        if((index*2)+2 < *length && queue[index*2 + 2].priority > queue[index].priority){
            queue[index] = queue[(index*2)+2];
            tempIndex = index*2 +2;
        }

        if(index == tempIndex){
            break;
        }

        index = tempIndex;
    }
    return temp;
}

void printQueue(struct process queue[], int length){
    for (int i = 0;i < length; i++){
        printf("\nProcess : %d \t Priority : %d", queue[i].processID, queue[i].priority);
    }
}

int main(){
    struct process queue[1000];
    int filled = 0;

    int choice = 0;
    while (choice != 6){
        printf("\n========================================================================================================");
        printf("\n1. Insert Process");
        printf("\n2. Increase Priority of Process");
        printf("\n3. Extract max priority Process");
        printf("\n4. Peak into max priority Process");
        printf("\n5. Print all processes");
        printf("\n6. Exit");
        printf("\nEnter Choice : ");
        scanf(" %d", &choice);

        int priority = 0, processID = 0;
        struct process temp;

        switch (choice) {
            case 1:
                printf("Enter Process ID and priority : ");
                scanf(" %d %d", &processID, &priority);
                insertInQueue(queue, &filled, 1000, processID, priority);
                break;
            case 2:
                printf("Enter Process ID and priority : ");
                scanf(" %d %d", &processID, &priority);
                increasePriority(queue, filled, processID, priority);
                break;
            case 3:
                temp = extractMax(queue, &filled);
                printf("\nProcess : %d \t Priority : %d", temp.processID, temp.priority);
                break;
            case 4:
                temp = peakIntoQueue(queue, filled);
                printf("\nProcess : %d \t Priority : %d", temp.processID, temp.priority);
                break;
            case 5:

                printf("\n%d Processes are ", filled);
                printQueue(queue, filled);
                break;
            case 6:
                printf("\n\nThank You !");
                break;
            default:
                printf("\n! Enter a valid choice");
        }
    }
}