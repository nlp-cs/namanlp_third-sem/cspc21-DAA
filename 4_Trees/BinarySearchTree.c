#include <stdio.h>
#include <stdlib.h>

struct node{
    int data;
    struct node * left;
    struct node * right;
    struct node * parent;
};

typedef struct node node;

node * createNode(int key, node * prev){
    node * temp = (node *)malloc(sizeof(node));
    temp->data = key;
    temp->left = NULL;
    temp->right = NULL;
    temp->parent = prev;
    return temp;
}


node * insertInBST(node * root, int key, node * parent){

    if(root == NULL){
        return createNode(key, parent);
    }

    if(key < root->data){
        root->left = insertInBST(root->left, key, root);
    }else{
        root->right = insertInBST(root->right, key, root);
    }

    return root;
}


void inorderTraversal(node * root){

    if(root != NULL){
        inorderTraversal(root->left);
        printf(" %d", root->data);
        inorderTraversal(root->right);
    }
}

int isLeaf(node * given){
    if(given->left == NULL && given->right==NULL){
        return 1;
    }
    return 0;
}

node * search(node * root, int key){
    if(root == NULL || root->data==key){
        return root;
    }
    if (key < root->data){
        return search(root->left, key);
    }
    return search(root->right, key);
}

int main(){
    node * root = NULL;

    int totalNodes = 0;
    printf("Enter number of nodes : ");
    scanf(" %d", &totalNodes);
    if(totalNodes<=0){
        printf("Thanks");
        return 0;
    }

    printf("Enter nodes : ");
    int NodeVal = 0;
    scanf(" %d", &NodeVal);

    root = insertInBST(NULL, NodeVal, NULL);
    for (int i = 1; i < totalNodes; i++ ){
        scanf(" %d", &NodeVal);
        insertInBST(root, NodeVal, root);
    }

    printf("Inorder traversal is : ");
    inorderTraversal(root);
    printf("\n");

    printf("Enter node to search : ");
    scanf(" %d", &NodeVal);

    node * foundNode = search(root, NodeVal);
    if(foundNode == NULL){
        printf("\nSorry, node not found !");
    } else{
        if(foundNode->parent == NULL){
            printf("\nGiven Node is the root node!");
        } else{
            printf("\nGive node is the child of : %d", foundNode->parent->data);
        }

        if(isLeaf(foundNode)){
            printf("\nYes, given node is a leaf node");
        } else{
            printf("\nNo, given node is not a leaf node!");
        }
    }
}
