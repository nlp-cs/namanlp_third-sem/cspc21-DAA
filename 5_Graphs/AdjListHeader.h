//
// Created by naman on 11/11/22.
//

#include <stdio.h>
#include <stdlib.h>

#ifndef INC_5_GRAPHS_ADJLISTHEADER_H
#define INC_5_GRAPHS_ADJLISTHEADER_H

struct adjacentVertex{
    int data;
    struct adjacentVertex * next;
};

struct vertex{
    int data;
    int visited;
    struct adjacentVertex * adjacentList;
};

// To print Adjacency list
void printAdjacencyList(struct vertex * adjList, int length){
    for (int i = 0; i < length; i++){
        printf("\n%d", adjList[i].data);
        struct adjacentVertex * iterator = adjList[i].adjacentList;
        while (iterator!=NULL){
            printf(" -> %d", iterator->data);
            iterator = iterator->next;
        }
    }
}

// To take edges of each node
void takeEdges(struct vertex * givenVertex){
    // Input number of edges
    int numberOfEdges = 0;
    printf("Enter number of edges of %d :", givenVertex->data);
    scanf(" %d", &numberOfEdges);
    // return if number of edges are 0
    if(numberOfEdges <=0){
        return;
    }

    struct adjacentVertex * iterator = NULL;
    printf("Enter edges of %d : ", givenVertex->data);

    //Traverse Adjacency List, if already present, else create new adjacency list
    int i = 0;
    if(givenVertex->adjacentList != NULL){
        iterator = givenVertex->adjacentList;
        while (iterator->next != NULL){
            iterator = iterator->next;
        }
    } else {
        givenVertex->adjacentList = (struct adjacentVertex *)malloc(sizeof(struct adjacentVertex));
        iterator = givenVertex->adjacentList;
        scanf(" %d", &iterator->data);
        iterator->next = NULL;
        // because we have already taken 1 edge
        i=1;
    }
    for(; i < numberOfEdges; i++){
        iterator->next = (struct adjacentVertex *)malloc(sizeof(struct adjacentVertex));
        iterator = iterator->next;
        scanf(" %d", &iterator->data);
        iterator->next = NULL;
    }
}


#endif //INC_5_GRAPHS_ADJLISTHEADER_H
