//
// Created by naman on 11/11/22.
//

#include "AdjListHeader.h"

void TopologicalSort(struct vertex adjList[], int totalVertex, int stackNum[], int pos){

    static int filled = 0;

    if(!adjList[pos].visited){
        adjList[pos].visited = 1;

        struct adjacentVertex * temp = adjList[pos].adjacentList;
        while (temp!=NULL){
            TopologicalSort(adjList, totalVertex, stackNum, temp->data);
            temp = temp->next;
        }
        stackNum[filled] = adjList[pos].data;
        filled++;
    }
}

int main(){
    printf("Enter number of vertices : ");
    int totalVertex = 0;
    scanf(" %d", &totalVertex);

    // Vertices are 0, 1.....n-1
    // Initialising adjacency list
    struct vertex adjacencyList[totalVertex];

    for (int i = 0; i < totalVertex; i++){
        adjacencyList[i].data = i;
        adjacencyList[i].visited = 0;
        adjacencyList[i].adjacentList = NULL;
    }

    for (int i = 0; i < totalVertex; i++){
        takeEdges(&adjacencyList[i]);
    }

    printf("\nOur adjacency list is : ");
    printAdjacencyList(adjacencyList, totalVertex);

    printf("\n Topological Sort is : ");
    int stackNum[totalVertex];

    for (int i = 0; i < totalVertex; i++){
        TopologicalSort(adjacencyList, totalVertex, stackNum, i);
    }

    for (int  i = totalVertex-1; i >= 0; i--){
        printf(" %d ", stackNum[i]);
    }


}
