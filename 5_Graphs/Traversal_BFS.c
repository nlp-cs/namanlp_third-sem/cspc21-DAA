//
// Created by naman on 11/10/22.
//

#include "AdjListHeader.h"

// ======================================================= Some Functions =====================================================================

struct vertex * findInQueue(struct vertex adjList[], int length, int key){
    for (int i = 0; i < length; i++){
        if(adjList[i].data == key){
            return &adjList[i];
        }
    }
    // If Not Found
    return NULL;
}

void enQueue(struct vertex*queue[] , int *filled, struct vertex * ptr){
    queue[*filled] = ptr;
    (*filled)++;
}

struct vertex * deQueue(struct vertex*queue[] , int *filled){
    if((*filled) <= 0){
        return NULL;
    }

    struct vertex * temp = queue[0];

    for (int i = 1; i < *filled; i++){
        queue[i-1] = queue[i];
    }

    (*filled)--;
    return temp;
}

// ====================================================== Actual BFS ========================================================

void BFS(struct vertex * adjList, int totalVertex, struct vertex * givenVertex,     struct vertex * queue[], int * filled){

        if(givenVertex->visited==0){
            givenVertex->visited=1;
            printf(" %d ", givenVertex->data);

            // Traverse the adjacent vertices, and put them in queue

            struct adjacentVertex * temp = givenVertex->adjacentList;
            while (temp!=NULL){
                struct vertex * ptr = findInQueue(adjList, totalVertex, temp->data);
                if(ptr!=NULL){
                    if(!ptr->visited)
                    enQueue(queue, filled, ptr);
                }
                temp = temp->next;
            }
            while (*filled!=0){
                BFS(adjList, totalVertex, deQueue(queue, filled), queue, filled);
            }
        }
}


int main(){
    printf("Enter number of vertices : ");
    int totalVertex = 0;
    scanf(" %d", &totalVertex);

    // Vertices are 0, 1.....n-1
    // Initialising adjacency list
    struct vertex adjacencyList[totalVertex];

    for (int i = 0; i < totalVertex; i++){
        adjacencyList[i].data = i;
        adjacencyList[i].visited = 0;
        adjacencyList[i].adjacentList = NULL;
    }

    for (int i = 0; i < totalVertex; i++){
        takeEdges(&adjacencyList[i]);
    }

    printf("\nOur adjacency list is : ");

    printAdjacencyList(adjacencyList, totalVertex);

    printf("\n BFS is : ");

    struct vertex * queue[totalVertex*10];
    int filled = 0;

    for (int i = 0; i < totalVertex; i++){
        BFS(adjacencyList, totalVertex, &adjacencyList[i], queue, &filled);
    }

}