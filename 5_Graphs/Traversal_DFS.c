//
// Created by naman on 11/11/22.
//

#include "AdjListHeader.h"

void DFS(struct vertex * adjList, int totalVertex, int pos){
        if(!adjList[pos].visited){
            printf(" %d", adjList[pos].data);
            adjList[pos].visited = 1;

            struct adjacentVertex * temp = adjList[pos].adjacentList;
            while (temp!=NULL){
                DFS(adjList, totalVertex, (*temp).data);
                temp = temp->next;
            }
        }
}

// ===================================================== Main =============================================================

int main(){
    printf("Enter number of vertices : ");
    int totalVertex = 0;
    scanf(" %d", &totalVertex);

    // Vertices are 0, 1.....n-1
    // Initialising adjacency list
    struct vertex adjacencyList[totalVertex];

    for (int i = 0; i < totalVertex; i++){
        adjacencyList[i].data = i;
        adjacencyList[i].visited = 0;
        adjacencyList[i].adjacentList = NULL;
    }

    for (int i = 0; i < totalVertex; i++){
        takeEdges(&adjacencyList[i]);
    }

    printf("\nOur adjacency list is : ");

    printAdjacencyList(adjacencyList, totalVertex);

    printf("\n DFS is : ");

    for (int i = 0; i < totalVertex; i++){
        DFS(adjacencyList, totalVertex, i);
    }
}