//
// Created by naman on 11/10/22.
//

#include <stdio.h>
#include <stdlib.h>

struct adjacentVertex{
    int data;
    struct adjacentVertex * next;
};

struct vertex{
    int data;
    struct adjacentVertex * adjacentList;
};

// To print Adjacency list
void printAdjacencyList(struct vertex * adjList, int length){
    for (int i = 0; i < length; i++){
        printf("\n%d", adjList[i].data);
        struct adjacentVertex * iterator = adjList[i].adjacentList;
        while (iterator!=NULL){
            printf(" -> %d", iterator->data);
            iterator = iterator->next;
        }
    }
}

// To take edges of each node
void takeEdges(struct vertex * givenVertex){
    // Input number of edges
    int numberOfEdges = 0;
    printf("Enter number of edges of %d :", givenVertex->data);
    scanf(" %d", &numberOfEdges);
    // return if number of edges are 0
    if(numberOfEdges <=0){
        return;
    }

    struct adjacentVertex * iterator = NULL;
    printf("Enter edges of %d : ", givenVertex->data);

    //Traverse Adjacency List, if already present, else create new adjacency list
    int i = 0;
    if(givenVertex->adjacentList != NULL){
        iterator = givenVertex->adjacentList;
        while (iterator->next != NULL){
            iterator = iterator->next;
        }
    } else {
        givenVertex->adjacentList = (struct adjacentVertex *)malloc(sizeof(struct adjacentVertex));
        iterator = givenVertex->adjacentList;
        scanf(" %d", &iterator->data);
        iterator->next = NULL;
        // because we have already taken 1 edge
        i=1;
    }
    for(; i < numberOfEdges; i++){
        iterator->next = (struct adjacentVertex *)malloc(sizeof(struct adjacentVertex));
        iterator = iterator->next;
        scanf(" %d", &iterator->data);
        iterator->next = NULL;
    }
}

int main(){
    printf("Enter number of vertices : ");
    int totalVertex = 0;
    scanf(" %d", &totalVertex);

    // Vertices are 0, 1.....n-1
    // Initialising adjacency list
    struct vertex adjacencyList[totalVertex];
    for (int i = 0; i < totalVertex; i++){
        adjacencyList[i].data = i;
        adjacencyList[i].adjacentList = NULL;
    }

    for (int i = 0; i < totalVertex; i++){
        takeEdges(&adjacencyList[i]);
    }

    printf("\nOur adjacency list is : ");

    printAdjacencyList(adjacencyList, totalVertex);
}