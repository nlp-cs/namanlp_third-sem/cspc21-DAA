//
// Created by naman on 11/10/22.
//

#include <stdio.h>

int main(){
    printf("Enter number of vertices : ");
    int totalVertex = 0;
    scanf(" %d", &totalVertex);

    // Initialize adjacency matrix
    int adjacencyMatrix[totalVertex][totalVertex];
    for(int i = 0; i < totalVertex; i++){
        for(int j = 0; j < totalVertex; j++){
            adjacencyMatrix[i][j] = 0;
        }
    }

    printf("\nEnter edges");
    int fromNode = 0, toNode = 0;
    while (1){
        printf("\nEnter from node (-1 to stop) : ");
        scanf(" %d", &fromNode);
        printf("Enter to node (-1 to stop) : ");
        scanf(" %d", &toNode);
        if(fromNode < 0 || toNode < 0){
            break;
        }
        if(fromNode >= totalVertex || toNode >= totalVertex){
            printf("\nSorry, out of bound! You can enter values from 0 to n-1 !");
            continue;
        }
        adjacencyMatrix[fromNode][toNode] = 1;
    }

    printf("Entered adjacency matrix is : \n");
    for (int i = 0; i < totalVertex; i++){
        printf("\n %d : ", i  );
        for(int j = 0; j < totalVertex; j++){
            printf(" %d ", adjacencyMatrix[i][j]);
        }
    }
}