//
// Created by naman on 25/11/22.
//

#include "weightedAdjacencyList.h"

void PrimeMST(struct vertex * adjList, int totalVertex){
    int coveredVertices = 0;

    int reach[totalVertex];
    int parent[totalVertex];
    for(int i = 0; i < totalVertex; i++){
        reach[i] = 1000000;
    }
    for(int i = 0; i < totalVertex; i++){
        parent[i] = -1;
    }
    reach[0] = 0;

    // We will run till all vertices are covered
    while (coveredVertices!=totalVertex){
        coveredVertices++;
        int minIndex = 0;
        for (int i = 0; i < totalVertex; i++){
            // checks if node is unvisited
            if(!adjList[i].visited && (reach[i]<reach[minIndex] || (adjList[minIndex].visited))){
                minIndex = i;
            }
        }

        // We have got the minimum index element

        printf("\n %d  -  %d weight  :  %d", parent[minIndex], minIndex, reach[minIndex]);
        adjList[minIndex].visited=1;

        struct adjacentVertex * traverse = adjList[minIndex].adjacentList;
        while (traverse!=NULL){
            if(reach[traverse->data] > traverse->weight && !adjList[traverse->data].visited){
                reach[traverse->data] = traverse->weight;
                parent[traverse->data] = minIndex;
            }
            traverse = traverse->next;
        }

    }
}

int main(){
    printf("Enter number of vertices : ");
    int totalVertex = 0;
    scanf(" %d", &totalVertex);

    // Vertices are 0, 1.....n-1
    // Initialising adjacency list
    struct vertex adjacencyList[totalVertex];

    for (int i = 0; i < totalVertex; i++){
        adjacencyList[i].data = i;
        adjacencyList[i].visited = 0;
        adjacencyList[i].adjacentList = NULL;
    }

    for (int i = 0; i < totalVertex; i++){
        takeEdges(&adjacencyList[i]);
    }

    printf("\nOur adjacency list is : ");

    printAdjacencyList(adjacencyList, totalVertex);

    printf("\nfrom - to weight");
    PrimeMST(adjacencyList, totalVertex);
}

// Input : 9
// 2 4 1 8 7 3 4 0 11 7 8 2 4 8 1 7 3 4 5 2 8  3 7 2 9 4 14 5 2 9 3 10 5 4 4 2 14 3 10 4 2 6 3 2 5 6 8 1 7 4 8 0 11 1 7 8 1 6 3 2 2 6 6 7 7