//
// Created by naman on 14/11/22.
//
#include <stdio.h>

int length = 0;

int max(int  a , int b){
    if(a>=b){
        return a;
    }
    return b;
}


int LCS(char s1[], int l1, char s2[], int l2, int arr[length][length]){

    if(l1<=0 || l2<=0){
        arr[l1][l2] = 0;
        return 0;
    }

    if(arr[l1][l2] != -1){
        return arr[l1][l2];
    }

    if(s1[l1-1] == s2[l2-1]){
        arr[l1][l2] =  1+LCS(s1, l1-1, s2, l2-1, arr);
        return arr[l1][l2];
    }

    arr[l1][l2] =  max(LCS(s1, l1, s2, l2-1, arr), LCS(s1, l1-1, s2, l2, arr));
    return arr[l1][l2];
}

int main(){
    printf("Enter length of strings : ");
    scanf(" %d", &length);

    char s1[length];
    char s2[length];
    printf("Enter first string : ");
    scanf(" %s", s1);
    printf("Enter second string : ");
    scanf(" %s", s2);

    int arr[length+1][length+1];

    for (int i = 0; i <= length; i++){
        for (int j = 0; j <= length; j++){
            arr[i][j] = -1;
        }
    }

    printf("Maximum length of LCS is : %d", LCS(s1, length, s2, length, arr));
}