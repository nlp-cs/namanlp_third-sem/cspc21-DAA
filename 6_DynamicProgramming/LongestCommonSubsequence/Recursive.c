//
// Created by naman on 14/11/22.
//

#include <stdio.h>

int max(int  a , int b){
    if(a>=b){
        return a;
    }
    return b;
}

int LCS(char s1[], int l1, char s2[], int l2){
    if(l1==0 || l2==0){
        return 0;
    }

    if(s1[l1-1] == s2[l2-1]){
        return 1+LCS(s1, l1-1, s2, l2-1);
    }

    return max(LCS(s1, l1, s2, l2-1), LCS(s1, l1-1, s2, l2));

}

int main(){
    printf("Enter length of strings : ");
    int length = 0;
    scanf(" %d", &length);

    char s1[length];
    char s2[length];
    printf("Enter first string : ");
    scanf(" %s", s1);
    printf("Enter second string : ");
    scanf(" %s", s2);

    printf("Maximum length of LCS is : %d", LCS(s1, length, s2, length));
}