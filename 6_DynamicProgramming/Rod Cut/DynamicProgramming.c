//
// Created by naman on 14/11/22.
//

#include <stdio.h>

int max(int  a , int b){
    if(a>=b){
        return a;
    }
    return b;
}

int maxProfit(int length, int rates[], int maxProfits[]){

    // Base case
    if(length==0 || length == 1){
        return rates[length];
    }

    int maxP = rates[length];

    for (int i = 1; i < length; i++){
        if(maxProfits[i] == -1){
            maxProfits[i] = maxProfit(i, rates, maxProfits);
        }
        if(maxProfits[length-i] == -1){
            maxProfits[length-i] = maxProfit(length-i, rates, maxProfits);
        }
        maxP = max(maxP,maxProfits[i] + maxProfits[length-i]);
    }

    return maxP;
}


int main(){
    int length = 0;
    printf("Enter length of rod : ");
    scanf(" %d", &length);

    printf("Enter %d rates ( 1 to %d ) : ", length, length);

    int rates[length+1];
    rates[0] = 0;
    for(int i = 1; i <= length; i++){
        scanf(" %d", &rates[i]);
    }

    int maxProfits[length+1];
    for (int i = 0; i <= length; i++){
        maxProfits[i] = -1;
    }
    maxProfits[0] = 0;

    printf("Maximum profit is : %d", maxProfit(length, rates, maxProfits));

}