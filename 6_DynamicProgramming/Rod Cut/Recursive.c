//
// Created by naman on 14/11/22.
//

#include <stdio.h>

int max(int  a , int b){
    if(a>=b){
        return a;
    }
    return b;
}

int maxProfit(int length, int rates[]){
    int maxP = rates[length];

    // Base case
    if(length==0 || length == 1){
        return rates[length];
    }

    for(int i = 1; i < length; i++){
        maxP = max(maxP,maxProfit(length-i, rates) + maxProfit(i, rates) );
    }
    return maxP;
}


int main(){
    int length = 0;
    printf("Enter length of rod : ");
    scanf(" %d", &length);

    printf("Enter %d rates ( 1 to %d ) : ", length, length);

    int rates[length+1];
    rates[0] = 0;
    for(int i = 1; i <= length; i++){
        scanf(" %d", &rates[i]);
    }

    printf("Maximum profit is : %d", maxProfit(length, rates));

}