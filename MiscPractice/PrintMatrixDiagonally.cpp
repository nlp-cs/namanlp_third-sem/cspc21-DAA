// This is a question from geeks for geeks. Refer 
// https://practice.geeksforgeeks.org/problems/print-diagonally4331/1

//{ Driver Code Starts

#include<bits/stdc++.h>
using namespace std;

// } Driver Code Ends
class Solution{

// ============================================================================================== My Code ==========================================================================
	
	public:
	vector<int> downwardDigonal(int N, vector<vector<int>> A)
	{
	    vector<int> answer;
	    // Print upper half triangle
	    for(int i = 0; i < N; i++){
	        for(int j = 0; j <= i ; j++){
	            answer.push_back(A.at(j).at(i-j));
	        }
	    }
	    
	    
	    for(int i = 1; i < N; i++){
	        for(int j = i; j < N; j++){
	            answer.push_back(A.at(j).at( (N-1) + i -j));
	        }
	    }
	    
	    return answer;
	}

// ====================================================================================================================================================================================

};

//{ Driver Code Starts.



int main()
{

    
    int t;
    cin >> t;
    while(t--) 
    {
        int n;
        cin >> n;

        vector<vector<int>> A(n, vector<int>(n));

        for(int i = 0; i < n; i++)
        	for(int j = 0; j < n; j++)
        		cin >> A[i][j];

        Solution obj;
        vector<int> ans = obj.downwardDigonal(n, A);

        for(auto i:ans)
        	cout << i << " ";

	    cout << "\n";
    }

    return 0;
}

// } Driver Code Ends
